# uxnería

experiments with [uxn](https://wiki.xxiivv.com/site/uxn.html) programming.

TODO:

* update projects to latest uxntal syntax
* create folder for the [uxn tutorial](https://compudanzas.net/uxn_tutorial.html) examples

# projects

* nibble dice tracker
* darena

## nibble dice tracker

`nibble-dice-tracker.tal` a 3-channel music tracker based on the nibble dice described by maleza.

[uxn nibble dice tracker - demo video](https://video.anartist.org/videos/watch/6d2d4a2c-78ef-48c8-abd0-e124451805cb)

[nibble dice tracker - info page](https://compudanzas.net/nibble_dice_tracker.html)

### interface

the interface consists in 3 sections, one for each audio channel/device.

each section consists in 2 columns. the right column of a section has an indicator for the position of the "beat", and its state (playing or not).

each column consists in 8 bytes.

a byte consists in 2 contiguous nibble dice: the one at the left is the *high* nibble, and the one at the right is the *low* nibble.

the bytes in a section map to the following aspects of the tracker and [audio device](https://wiki.xxiivv.com/site/uxnemu.html#audio):

* period ( 1 byte ) - how many frames have to happen to trigger a beat. 
	- if 0, the device is off
* pattern ( 8 bits ) - each bit indicates if a note has to be played or not in that beat
	- bit 0 (least significant bit) corresponds to beat 0, bit 1 to beat 1, and so on.
* beatcount ( 2 nibbles ) - definition of the beat count behavior
	- low nibble: how many beats happen in a cycle. if 0, there are no beats.
	- high nibble: beat offset; starting value for the beat count.
	- e.g. if high nibble is 2, and low nibble is 4, the beat count will repeat as 2, 3, 4, 5
* adsr ( 2 bytes ) - envelope values, as described by the [audio device](https://wiki.xxiivv.com/site/uxnemu.html#audio) reference
* volume ( 2 nibbles ) - left and right volume values, as described by the audio device reference
* sample ( 2 nibbles and 1 byte ) - definition of the sample to be used
	- high nibble: index of sample to use:
		* 0: saw
		* 1: main (program code)
		* 2: piano
		* 3: triangular
	- low nibble + byte: length of the sample to use
* melody ( 8 bytes ) - [midi](https://wiki.xxiivv.com/site/midi.html) notes corresponding to beats 0 to 7, from top to bottom.


### controls

* Arrow key up, or `k` : move cursor to previous byte
* Arrow key down, or `j`: move cursor to next byte
* Arrow key left, or `h`: move cursor to previous nibble
* Arrow key right, or `l`: move cursor to next nibble
* `Ctrl` or `+`: increment nibble
* `Alt` or `-`: decrement nibble
* `0` to `9` and `a` to `f`: assign value to nibble

### nibble dice references

* [no binario - pachapunk](https://pachapunk.space/nobinario.html)
* [XXIIVV nibble dice](https://wiki.xxiivv.com/site/nibble_dice.html)

### possible improvements

* midi output?
* ~~automatically center interface on screen~~ (done!)
* ~~keys for setting the value of a nibble?~~ (done!)

## darena

`darena.tal` an open-ended game of rocks and sand.

[darena info page](https://compudanzas.net/darena.html)

### possible "improvements"

* add an "official" pulling option (you can pull rocks right now, but in a hacky way)
* save rocks state (position and color) to file
* optimize the use of memory for rock colors, using one bit per rock instead of one byte
* optimize for 256x192 screensize

# roms

ready-assembled projects:

* `nibble-dice-tracker.rom`
* `darena.rom`

# sketches

* `nibble.tal`: small demo drawing all the nibble dice
* `audio-prototype0.tal`: first iteration of sequencer / tracker with nibble dice. the dice are used to read/write into memory:
	- you can manipulate the dice in order to change pulse period (1 byte), pitch (1 byte), adsr values (2 bytes), volume (1 byte), "bang" sequencing (?) (1 byte); to toggle a melody sequencer (1 bit), and/or modify the melody (8 bytes), and even the super low-res wave shape (8 bytes), for two audio devices (3 columns for the first one, 3 columns for the second one)
* `calculadora.tal`: experiment for an 8-bit rpn calculator, from before there was a console/vector available. for the moment it uses the keyboard input

# media

* `nibble-dice.chr` is the spritesheet for the nibble dice. it has them in an 8x8 pixels version, and in 16x16. 

drawn using [nasu](https://100r.co/site/nasu.html)

# license

sejo - cc0: dedicated to the public domain
